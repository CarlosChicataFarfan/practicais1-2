package service;

import java.util.Collection;
import repository.AlumnoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import domain.Alumno;

@Service
public class AlumnoService {
	
	@Autowired
	AlumnoRepository AlumnoR;
	
	@Transactional
	public void SaveAlumno( Alumno alumno ){
		AlumnoR.persist(alumno);
	}
	
	@Transactional
	public Alumno GetAlumnoById( Long Id ){
		return AlumnoR.find( Id );
	}
	
	@Transactional
	public Collection< Alumno> GetAllAlumnoUpdate(){
		return AlumnoR.findAll();
	}
}
