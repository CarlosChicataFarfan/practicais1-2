package service;

import java.util.Collection;
import repository.CursoRepository;
import domain.Curso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CursoService {

	@Autowired
	CursoRepository CursoR;
	
	@Transactional
	public void SaveCurso( Curso salvar ){
		CursoR.persist(salvar);
		return ;
	}
	
	@Transactional
	public Collection< Curso > GetAllCursoUpdate(){
		return CursoR.findAll();
	}
	
	@Transactional
	public Curso GetCursoById( Long id ){
		return CursoR.find(id);
	}
}
