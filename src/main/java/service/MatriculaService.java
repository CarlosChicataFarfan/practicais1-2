package service;

import domain.Matricula;
import repository.MatriculaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collection;
import domain.Alumno;

@Service
public class MatriculaService {
	@Autowired
	MatriculaRepository MatriculaS;
	
	@Transactional
	public Collection< Alumno> GetAlumnoBySemestreAndCurso( String semestre, String cursos ){
		return MatriculaS.FindAlumnoBySemestreAndCurso(semestre, cursos);
	}
	
	@Transactional
	public void SaveMatricula( Matricula matricula ){
		MatriculaS.persist(matricula);
	}
	
	@Transactional
	public void RemoveMatricula( Matricula matricula){
		MatriculaS.remove(matricula);
	}
	
}
