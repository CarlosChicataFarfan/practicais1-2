package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.AlumnoService;
import domain.Alumno;

@Controller
public class AlumnoController {

		@Autowired
		AlumnoService AlumnoS;
		
		@RequestMapping( value = "SaveAlumno" , method = RequestMethod.POST )
		String SaveCurso(@ModelAttribute Alumno alumnos, ModelMap modelo ){
			AlumnoS.SaveAlumno(alumnos);
			modelo.addAttribute( "AlumnosActuales", AlumnoS.GetAllAlumnoUpdate() );
			return "Alumno";
		}
		
		@RequestMapping( value = "AddAlumno" , method = RequestMethod.GET )
		String AddNewCurso( @RequestParam( required = false) Long Id , ModelMap modelo ){
			Alumno alumnonuevo = Id == null ? new Alumno() : AlumnoS.GetAlumnoById(Id);
			modelo.addAttribute("Alumno", alumnonuevo );
			return "add-alumno";
		}
		
		@RequestMapping( value = "InicioAlumno" , method = RequestMethod.GET )
		String Show(@RequestParam( required = false ) Long id , ModelMap modelo ){	
			modelo.addAttribute("AlumnoActuales", AlumnoS.GetAllAlumnoUpdate() );
			return "Alumno";
		}
		
		
		
}
