package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import repository.MatriculaRepository;

@Controller
public class MatriculaController {

	@Autowired
	MatriculaRepository MatriculaS;
	
	@RequestMapping( value = "InicioMatricula" , method = RequestMethod.GET )
	String Show( @RequestParam( required = false ) Long ids , ModelMap modelo ){
		return "Matricula";
	}
}
