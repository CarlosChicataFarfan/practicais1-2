package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import service.CursoService;
import domain.Curso;

@Controller
public class CursoController {

	@Autowired
	CursoService CursoS;
	
	@RequestMapping( value = "SaveCurso" , method = RequestMethod.POST )
	String SaveCurso(@ModelAttribute Curso curso, ModelMap modelo ){
		CursoS.SaveCurso(curso);
		modelo.addAttribute("CursoDisponibles", CursoS.GetAllCursoUpdate() );
		return "Curso";
	}
	
	@RequestMapping( value = "AddCurso" , method = RequestMethod.GET )
	String AddNewCurso( @RequestParam( required = false) Long Id , ModelMap modelo ){
		Curso cursonuevo = Id == null ? new Curso() : CursoS.GetCursoById(Id);
		modelo.addAttribute("Curso", cursonuevo );
		return "add-curso";
	}
		
	@RequestMapping( value = "InicioCurso" , method = RequestMethod.GET )
	String Show(@RequestParam( required = false ) Long id , ModelMap modelo ){
		modelo.addAttribute("CursoDisponibles", CursoS.GetAllCursoUpdate() );
		return "Curso";
	}


}
