package repository.jpa;


import domain.Curso;
import repository.CursoRepository;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

@Repository
public class JpaCursoRepository extends JpaBaseRepository< Curso, Long> implements CursoRepository{

	@Override
	public Curso FindCursoByCodigo( String Codigo_ ){
		String query = "Select a FROM Curso a WHERE a.codigo = :Codigo_";
		TypedQuery< Curso > consulta = entityManager.createQuery(query, Curso.class);
		return consulta.getSingleResult();
	}
	
	@Override
	public Curso FindCursoByPartOfTitle( String title ){
		String query = " SELECT a FROM Curso WHERE LOCATE( title , a.nombre) != 0";
		TypedQuery< Curso > consulta = entityManager.createQuery(query, Curso.class );
		return consulta.getSingleResult();
	}
}
