package repository.jpa;

import java.util.Collection;
import repository.AlumnoRepository;
import domain.Alumno;
import org.springframework.stereotype.Repository;
import javax.persistence.TypedQuery;

@Repository
public class JpaAlumnoRepository extends JpaBaseRepository< Alumno, Long> implements AlumnoRepository{
	public Collection< Alumno > GetAlumnoByApellidoPaterno( String apellido ){
		String query = "SELECT a FROM ALUMNO a WHERE a.apellidoPaterno = :apellido";
		TypedQuery< Alumno > consulta = entityManager.createQuery( query, Alumno.class );
		return consulta.getResultList();
	}
}
