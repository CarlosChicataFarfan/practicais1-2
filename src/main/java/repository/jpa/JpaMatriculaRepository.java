package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import domain.Alumno;
import domain.Matricula;
import repository.MatriculaRepository;


@Repository
public class JpaMatriculaRepository extends JpaBaseRepository< Matricula, Long > implements MatriculaRepository{
	public Collection< Alumno> FindAlumnoBySemestreAndCurso( String semestre , String Curson ){
		String Query = "SELECT a FROM Matricula a WHERE a.semestre = :semestre AND a.curso.nombre = :Curson";
		TypedQuery< Alumno > consulta = entityManager.createQuery( Query , Alumno.class );
		return consulta.getResultList();
	}
}
