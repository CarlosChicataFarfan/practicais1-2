package repository;

import domain.Curso;

public interface CursoRepository extends BaseRepository< Curso , Long >{

	Curso FindCursoByCodigo( String Codigo_ );
	Curso FindCursoByPartOfTitle( String title );
}
