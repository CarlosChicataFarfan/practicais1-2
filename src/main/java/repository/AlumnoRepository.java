package repository;

import java.util.Collection;
import domain.Alumno;


public interface AlumnoRepository extends BaseRepository< Alumno, Long >{
	Collection< Alumno > GetAlumnoByApellidoPaterno( String apellido );
	
}
