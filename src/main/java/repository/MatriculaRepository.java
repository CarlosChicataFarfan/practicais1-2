package repository;

import domain.Matricula;
import java.util.Collection;
import domain.Alumno;

public interface MatriculaRepository extends BaseRepository< Matricula, Long >{
		Collection< Alumno> FindAlumnoBySemestreAndCurso( String semestre , String Curso );
}
