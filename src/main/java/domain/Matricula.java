package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.OneToOne;


@Entity
public class Matricula implements BaseEntity<Long> {
	@Id
	@SequenceGenerator( name = "MatriculaIdSequence" , allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE , generator = "MatriculaIdSequence" )
	private Long id;

	@OneToOne
	private Curso curso;

	@OneToOne
	private Alumno alumno;

	@Column( precision = 2 )
	private Double nota;

	@Column( length = 64 )
	private String semestre;

	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
		this.nota = nota;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

}
